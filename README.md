# VS Code AspectJ Extension

This is a Visual Studio Code extension which acts as a client for the [AspectJ Language Server](https://gitlab.com/tsp_project/aspectj_ls).
It was mostly auto-generated using the [VSCode extension generator](https://code.visualstudio.com/api/get-started/your-first-extension) and then sligthly modified to work with the AspectJ Language Server.

This client in responsible to launch the language server.
In order to do this it is necessary to define two environemnt variables: one which points to the Java jdk directory in your system and the other which points to the `target` directory in the AspectJ Language Server's repo.

For example:
```bash
JAVA_HOME="/usr/lib/jvm/jdk-14.0.1"
AspectJLS_HOME="<path_to_aspectj_ls>/launcher/target"
```

## Install
Open the extension root directory with your terminal, then:
```bash
> npm install
> npm run compile
```

## Run
First of all, be sure that the AspectJ Language Server is compiled on your system.

Then, always from the root directory of the extension:
```bash
> code --extensionDevelopmentPath=$(pwd)
```
