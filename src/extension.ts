/* --------------------------------------------------------------------------------------------
 * From: https://gist.githubusercontent.com/NipunaMarcus/175feb33ffb7acc7762b75fd6a53657f/raw/272c2c43c9571cbbb35c769afbf584d2b06fb76b/helloFalks-%3Esrc-%3Eextension.ts
 * ------------------------------------------------------------------------------------------ */

import * as path from 'path';
import * as vscode from 'vscode';

// Import the language client, language client options and server options from VSCode language client.
import { LanguageClient, LanguageClientOptions, ServerOptions } from 'vscode-languageclient/node';

// Name of the launcher class which contains the main.
const main: string = 'ServerLauncher';

export function activate(context: vscode.ExtensionContext) {
	console.log('Extension "AspectJ-extension" is now active!');

	// Get the java home from the process environment.
	const { JAVA_HOME } = process.env;
	const { AspectJLS_HOME } = process.env;

	// If java home is available continue.
	if (JAVA_HOME && AspectJLS_HOME ) {
		console.log(`JAVA_HOME: ${JAVA_HOME}\nAspectJLS_HOME: ${AspectJLS_HOME}`)

		// Java execution path.
		let executable: string = path.join(JAVA_HOME, 'bin', 'java');

		// path to the launcher.jar
		let classPath = path.join(AspectJLS_HOME, 'launcher.jar');
		const args: string[] = ['-cp', classPath];

		// Set the server options 
		// -- java execution path
		// -- argument to be pass when executing the java command
		let serverOptions: ServerOptions = {
			command: executable,
			args: [...args, main],
			options: {}
		};

		// Options to control the language client
		let clientOptions: LanguageClientOptions = {
			// Register the server for AspectJ and Java documents
			documentSelector: [{ scheme: 'file', language: 'aspectj'}, {scheme: 'file', language: 'java'}]
		};

		// Create the language client and start the client.
		let client = new LanguageClient('AspectJLS', 'AspectJ Language Server', serverOptions, clientOptions);
		let disposable = client.start();

		// Disposables to remove on deactivation.
		context.subscriptions.push(disposable);

		const requestWeaveInfoCalculation = () => {
			client.sendRequest("calculateWeaveInfo");
		};

		context.subscriptions.push(vscode.commands.registerCommand("calculateWeaveInfo", requestWeaveInfoCalculation));
	}
}

// this method is called when your extension is deactivated
export function deactivate() { 
	console.log('Your extension "AspectJ-extension" is now deactivated!');
}
